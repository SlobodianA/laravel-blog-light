@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($errors->any())
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="card-title">You are logged in!</h5>
                        @if(auth()->user()->isAdmin())
                        <p class="card-text">
                            <a href="{{route('blog.admin')}}">Go to admin panel</a>
                        </p>
                        @endif
                        <p class="card-text">
                            <a href="{{route('post.blog.index')}}">View all post</a><br>
                            <a href="{{route('post.categories.index')}}">View all categories</a><br>
                            <a href="{{route('post.authors.index')}}">View all authors</a><br>
                        </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
