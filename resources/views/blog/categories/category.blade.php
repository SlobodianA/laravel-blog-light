@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-light" href="{{route('post.blog.index')}}">&larr; All posts</a>
                </nav>

                <div class="row">
                    <div class="col-md-8 mb-3">
                        <div class="row justify-content-center">
                            @foreach($paginator as $item)
                                <a href="{{route('post.blog.show',$item->id)}}" style="text-decoration: none; color: black">
                                    <div class="col-md-12 mb-3">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">{{$item->title}}</h4>
                                                <h5 class="card-subtitle mb-3">{{$item->excerpt}}</h5>
                                                <p class="card-text">{{substr($item->content_html,0,255)}}...</p>
                                            </div>
                                            <div class="card-body">
                                                <p class="card-text"> Author: <a href="#" class="card-link">{{$item->user->name}}</a></p>
                                            </div>
                                            <div class="card-footer d-flex justify-content-between">
                                                <span>Category: <a href="{{route('post.categories.show',$item->category->id)}}" class="card-link">{{$item->category->title}}</a></span>
                                                <span>Published Date: {{$item->published_at}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-3">
                        @include('blog.includes.toplist')
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    {{$paginator->links() }}
                </div>
            </div>
        @endif
    </div>
@endsection