@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-light" href="{{route('post.blog.index')}}">&larr; All posts</a>
                </nav>

                <div class="row">
                    <div class="col-md-8 mb-3">
                        <div class="row justify-content-center">
                            <div class="col-md-12 mb-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">{{$item->title}}</h4>
                                        <h5 class="card-subtitle mb-3">{{$item->excerpt}}</h5>
                                        <p class="card-text">{{$item->content_html}}</p>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text"> Author: <a href="{{route('post.authors.show',$item->user_id)}}" class="card-link">{{$item->user->name}}</a></p>
                                    </div>
                                    <div class="card-footer d-flex justify-content-between">
                                        <span>Category: <a href="{{route('post.categories.show',$item->category->id)}}" class="card-link">{{$item->category->title}}</a></span>
                                        <span>Published Date: {{$item->published_at}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row justify-content-center align-bottom mb-5">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">Article info</div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            Category: <a href="{{route('post.categories.show',$item->category->id)}}">{{$item->category->title}}</a>
                                        </li>
                                        <li class="list-group-item">Author: {{$item->user->name}}</li>
                                        <li class="list-group-item">Published date: {{$item->published_at}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @include('blog.includes.toplist')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection