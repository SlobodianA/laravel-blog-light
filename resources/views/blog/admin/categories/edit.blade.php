@extends('layouts.app')

@section('content')
    @php
        /** @var \App\Models\BlogCategory $item */
        /** @var \Illuminate\Support\ViewErrorBag $errors */
    @endphp
    <div class="container">
    @if($item->exists)
        <form action="{{route('blog.admin.categories.update', $item->id)}}" method="post">
        @method('PATCH')
    @else
        <form action="{{route('blog.admin.categories.store', $item->id)}}" method="post">
    @endif
        @csrf
        @if($errors->any())
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        @endif

        @if(session('success'))
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{session()->get('success')}}
                    </div>
                </div>
            </div>
        @endif
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('blog.admin.categories.includes.item_edit_main_col')
                    @if(! $item->exists)
                    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                        <a class="btn btn-light" href="{{route('blog.admin.categories.index')}}">&larr; Back</a>
                    </nav>
                    @endif
                </div>
                <div class="col-md-3">
                    @include('blog.admin.categories.includes.item_edit_add_col')
                </div>
            </div>
    </form>
    @if($item->exists)
        <br>
        <form action="{{route('blog.admin.categories.destroy', $item->id)}}" method="post">
            @method('DELETE')
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card card-block">
                        <div class="card-body nl-auto">
                            @if($item->id == 1)
                                <span>Can't delete root category</span>
                            @else
                                <button type="submit" class="btn btn-danger">DELETE</button>
                            @endif
                        </div>
                    </div>
                    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                        <a class="btn btn-light" href="{{route('blog.admin.categories.index')}}">&larr; Back</a>
                    </nav>
                </div>
                <div class="col-md-3"></div>
            </div>
        </form>
    @endif
    </div>
@endsection