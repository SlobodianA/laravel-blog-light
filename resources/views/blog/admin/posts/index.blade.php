@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-light" href="{{route('blog.admin')}}">&larr; Back</a>
                    <a class="btn btn-primary" href="{{route('blog.admin.posts.create')}}">Add post</a>
                </nav>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <td>#</td>
                            <td>Author</td>
                            <td>Category</td>
                            <td>Title</td>
                            <td>Published at</td>
                            </thead>
                            <tbody>
                            @foreach($paginator as $post)
                                @php /** @var \App\Models\BlogPost $post */@endphp
                                <tr @if(!$post->is_published) style="color:#ccc" @endif>
                                    <td>{{$post->id}}</td>
                                    <td>{{$post->user->name}}</td>
                                    <td>{{$post->category->title}}</td>
                                    <td>
                                        <a href="{{route('blog.admin.posts.edit', $post->id)}}">
                                            {{$post->title}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$post->published_at ? \Carbon\Carbon::parse($post->published_at)->format('Y-m-d H:i:s') : ''}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{$paginator->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection