@extends('layouts.app')

@section('content')
    @php
        /** @var \App\Models\BlogPost $item */
        /** @var \Illuminate\Support\ViewErrorBag $errors */
    @endphp
    <div class="container">
        @include('blog.admin.posts.includes.result_messages')
        @if($item->exists)
            <form action="{{route('blog.admin.posts.update', $item->id)}}" method="post">
            @method('PATCH')
        @else
            <form action="{{route('blog.admin.posts.store', $item->id)}}" method="post">
        @endif
            @csrf
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        @include('blog.admin.posts.includes.item_edit_main_col')
                        @if(!$item->exists)
                        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                            <a class="btn btn-light" href="{{route('blog.admin.posts.index')}}">&larr; Back</a>
                        </nav>
                        @endif
                    </div>
                    <div class="col-md-3">
                        @include('blog.admin.posts.includes.item_edit_add_col')
                    </div>
                </div>
            </form>
            @if($item->exists)
            <br>
            <form action="{{route('blog.admin.posts.destroy', $item->id)}}" method="post">
            @method('DELETE')
            @csrf
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card card-block">
                            <div class="card-body nl-auto">
                                <button type="submit" class="btn btn-danger">DELETE</button>
                            </div>
                        </div>
                        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                            <a class="btn btn-light" href="{{route('blog.admin.posts.index')}}">&larr; Back</a>
                        </nav>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </form>
            @endif
    </div>
@endsection