<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'John Doe',
                'email' => 'johndoe@mail.com',
                'password' => bcrypt('johndoe1'),
                'type' => 'admin',
            ],
            [
                'name' => 'Kate Laft',
                'email' => 'katelaft@mail.com',
                'password' => bcrypt('katelaft1'),
                'type' => 'default',
            ],
        ];

        DB::table('users')->insert($data);
    }
}
