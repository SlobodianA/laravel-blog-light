<?php
/**
 * Created by PhpStorm.
 * User: Slobodian
 * Date: 17.11.2019
 * Time: 17:08
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogPostUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:200',
            'slug' => 'max:200',
            'excerpt' => 'max:500',
            'content_raw' => 'string|max:10000|min:5|required',
            'category_id' => 'required|integer|exists:blog_categories,id',
        ];
    }
}