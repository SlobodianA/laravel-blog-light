<?php

namespace App\Http\Controllers\Blog;

use App\Repositories\BlogCategoryRepository;
use App\Repositories\BlogPostRepository;

class PostController extends BaseController
{
    /**
     * @var BlogPostRepository
     */
    private $blogPostRepository;
    /**
     * @var BlogCategoryRepository
     */
    private $blogCategoryRepository;

    public function __construct()
    {
        $this->blogPostRepository = app(BlogPostRepository::class);
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->blogPostRepository->getAllPublishedWithPaginate();
        $categoryList = $this->blogCategoryRepository->getCategoryList();
        $postsPerCategory = $this->blogPostRepository->getPostsPerCategory();
        $postsPerAuthor = $this->blogPostRepository->getPostsPerAuthor();
        return view('blog.posts.index', compact('paginator', 'categoryList', 'postsPerCategory', 'postsPerAuthor'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->blogPostRepository->getEdit($id);
        if(empty($item)){
            abort(404);
        }
        $postsPerCategory = $this->blogPostRepository->getPostsPerCategory();
        $postsPerAuthor = $this->blogPostRepository->getPostsPerAuthor();
        return view('blog.posts.post',compact('item','postsPerCategory', 'postsPerAuthor'));
    }
}
