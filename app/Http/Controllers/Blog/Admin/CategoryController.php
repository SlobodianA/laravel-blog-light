<?php

namespace App\Http\Controllers\Blog\Admin;

use App\Repositories\BlogCategoryRepository;
use App\Models\BlogCategory;
use Illuminate\Support\Str;
use App\Http\Requests\{
    BlogCategoryCreateRequest,
    BlogCategoryUpdateRequest
};

class CategoryController extends BaseController
{
    /**
     * @var BlogCategoryRepository
     */
    private $blogCategoryRepository;

    public function __construct()
    {
        parent::__construct();
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->blogCategoryRepository->getAllWithPaginate(5);
        return view('blog.admin.categories.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new BlogCategory();
        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.categories.edit', compact('item', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\BlogCategoryCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryCreateRequest $request)
    {
        $data = $request->input();

        $item = (new BlogCategory())->create($data);

        if($item){
            return redirect()
                ->route('blog.admin.categories.edit', [$item->id])
                ->with(['Success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Failed to save'])
                ->withInput();
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->blogCategoryRepository->getEdit($id);
        if(empty($item)){
            abort(404);
        }

        $categoryList = $this->blogCategoryRepository->getForComboBox();
        return view('blog.admin.categories.edit', compact('item', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\BlogCategoryUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCategoryUpdateRequest $request, $id)
    {

        $item = $this->blogCategoryRepository->getEdit($id);
        if(empty($item)){
            return back()->withErrors(['msg' => 'Entity with id=[{$id}] not found'])->withInput();
        }

        $data = $request->all();

        $result = $item->update($data);

        if($result){
            return redirect()->route('blog.admin.categories.edit', $item->id)
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()->withErrors(['msg' => 'Failed to save'])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = BlogCategory::destroy($id);

        if($result){
            return redirect()
                ->route('blog.admin.categories.index')
                ->with(['success' => "Category with ID: [$id] was deleted"]);
        } else {
            return back()->withErrors(['msg' => 'Cant delete the post']);
        }
    }
}
