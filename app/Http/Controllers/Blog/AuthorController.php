<?php
/**
 * Created by PhpStorm.
 * User: Slobodian
 * Date: 24.11.2019
 * Time: 19:17
 */

namespace App\Http\Controllers\Blog;

use App\Repositories\BlogCategoryRepository;
use App\Repositories\BlogPostRepository;

class AuthorController extends BaseController
{
    /**
     * @var BlogPostRepository
     */
    private $blogPostRepository;

    /**
     * @var BlogCategoryRepository
     */
    private $blogCategoryRepository;

    public function __construct()
    {
        $this->blogPostRepository = app(BlogPostRepository::class);
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->blogPostRepository->getAllAuthors();
        $categoryList = $this->blogCategoryRepository->getCategoryList();
        $postsPerCategory = $this->blogPostRepository->getPostsPerCategory();
        $postsPerAuthor = $this->blogPostRepository->getPostsPerAuthor();
        return view('blog.authors.index', compact('paginator', 'categoryList', 'postsPerAuthor', 'postsPerCategory'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paginator = $this->blogPostRepository->getAuthorPostsWithPaginate($id, 5);
        $postsPerCategory = $this->blogPostRepository->getPostsPerCategory();
        $postsPerAuthor = $this->blogPostRepository->getPostsPerAuthor();

        return view('blog.authors.author', compact('paginator', 'postsPerAuthor', 'postsPerCategory'));
    }
}