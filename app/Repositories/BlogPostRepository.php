<?php
/**
 * Created by PhpStorm.
 * User: Slobodian
 * Date: 16.11.2019
 * Time: 18:29
 */

namespace App\Repositories;

use App\Models\BlogPost as Model;

class BlogPostRepository extends CoreRepository
{
    /**
     * @return mixed|string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $page
     * @return mixed
     */
    public function getAllWithPaginate($page = 25)
    {
        $fields = [
            'id',
            'title',
            'slug',
            'is_published',
            'published_at',
            'user_id',
            'category_id'
        ];

        $result = $this
            ->startConditions()
            ->select($fields)
            ->with(['category:id,title','user:id,name'])
            ->orderBy('id','DESC')
            ->paginate($page);

        return $result;
    }

    /**
     * @param int $page
     * @return mixed
     */
    public function getAllPublishedWithPaginate($page = 10)
    {
        $fields = [
            'id',
            'title',
            'published_at',
            'user_id',
            'category_id',
            'excerpt',
            'content_html'
        ];

        $result = $this
            ->startConditions()
            ->select($fields)
            ->with(['category:id,title','user:id,name'])
            ->where('is_published', 1)
            ->orderBy('published_at','DESC')
            ->paginate($page);

        return $result;
    }

    /**
     * @return mixed
     */
    public function getPostsPerCategory()
    {
        $fields = implode(',', [
            'category_id',
            'COUNT(*) AS quantity'
        ]);

        $result = $this
            ->startConditions()
            ->selectRaw($fields)
            ->with(['category:id,title'])
            ->groupBy('category_id')
            ->orderBy('quantity', 'DESC')
            ->limit(5)
            ->get();

        return $result;
    }

    public function getPostsPerAuthor()
    {
        $fields = implode(',', [
            'user_id',
            'COUNT(*) AS quantity'
        ]);

        $result = $this
            ->startConditions()
            ->selectRaw($fields)
            ->with(['user:id,name'])
            ->limit(3)
            ->orderBy('quantity', 'DESC')
            ->groupBy('user_id')
            ->get();

        return $result;
    }

    /**
     * @param int $page
     * @return mixed
     */
    public function getAllAuthors($page = 10)
    {

        $fieldsRaw = implode(',', [
            'user_id',
            'COUNT(*) AS quantity'
        ]);


        $result = $this
            ->startConditions()
            ->selectRaw($fieldsRaw)
            ->with(['user:id,name'])
            ->orderBy('user_id','ASC')
            ->groupBy('user_id')
            ->paginate($page);
        return $result;
    }

    /**
     * @param int $id
     * @param int $page
     * @return mixed
     */
    public function getCategoryPostsWithPaginate($id, $page = 10)
    {
        $fields = [
            'id',
            'title',
            'published_at',
            'user_id',
            'category_id',
            'excerpt',
            'content_html'
        ];

        $result = $this
            ->startConditions()
            ->select($fields)
            ->with(['category:id,title','user:id,name'])
            ->where([
                ['is_published', 1],
                ['category_id', $id]
            ])
            ->orderBy('published_at','DESC')
            ->paginate($page);

        return $result;
    }

    /**
     * @param int $id
     * @param int $page
     * @return mixed
     */
    public function getAuthorPostsWithPaginate($id, $page = 10)
    {
        $fields = [
            'id',
            'title',
            'published_at',
            'user_id',
            'category_id',
            'excerpt',
            'content_html'
        ];

        $result = $this
            ->startConditions()
            ->select($fields)
            ->with(['category:id,title','user:id,name'])
            ->where([
                ['is_published', 1],
                ['user_id', $id]
            ])
            ->orderBy('published_at','DESC')
            ->paginate($page);

        return $result;
    }
    /**
     * @param int $id
     * @return Model
     */
    public function getEdit($id)
    {
        $result = $this->startConditions()->find($id);

        return $result;
    }

    public function getAllCategoriesWithPaginate($perPage = null)
    {
        $columns = ['id', 'title', 'parent_id'];

        $result = $this
            ->startConditions()
            ->selectRaw($columns)
            ->with(['parentCategory:id,title', 'category:id,title'])
            ->paginate($perPage);

        return $result;
    }
}