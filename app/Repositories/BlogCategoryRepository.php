<?php

namespace App\Repositories;

use App\Models\BlogCategory as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BlogCategoryRepository
 * @package App\Repositories
 */
class BlogCategoryRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * @return Collection
     */
    public function getForComboBox()
    {
        $fields = implode(',', [
            'id',
            'CONCAT(id, ". ", title) AS id_title'
        ]);

        $result = $this
            ->startConditions()
            ->selectRaw($fields)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * @return Collection
     */
    public function getCategoryList()
    {
        $fields = [
            'id',
            'title'
        ];

        $result = $this
            ->startConditions()
            ->select($fields)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * @param null $perPage
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = [
            'id',
            'title',
            'parent_id',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->with(['parentCategory:id,title'])
            ->paginate($perPage);

        return $result;
    }

    public function getAllWithPaginateAndPostQuantity($perPage = null)
    {
        $columns = implode(',',[
            'blog_categories.id',
            'blog_categories.title',
            'blog_categories.parent_id',
            'COUNT(blog_posts.id) AS quantity'
        ]);

        $result = $this
            ->startConditions()
            ->leftJoin('blog_posts', 'blog_posts.category_id', '=', 'blog_categories.id')
            ->groupBy('blog_posts.category_id')
            ->selectRaw($columns)
            ->with(['parentCategory:id,title'])
            ->paginate($perPage);

        return $result;
    }
}