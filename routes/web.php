<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
// Standard laravel page
Route::get('/', 'Blog\PostController@index');

//Routes for login/logout
Auth::routes();

//home page
Route::get('/home', 'HomeController@index')->name('home');

//Admin panel main page
Route::get('/admin/blog', 'Blog\Admin\AdminController@index')->middleware('is_admin')->name('blog.admin');

//Posts page
Route::group(['namespace' => 'blog', 'prefix' => 'blog'], function(){
    Route::resource('posts', 'PostController')->only('index', 'show')->names('post.blog');
});

//Category page
Route::group(['namespace' => 'blog', 'prefix' => 'blog'], function(){
    Route::resource('categories', 'CategoryController')
        ->only('index', 'show')
        ->names('post.categories');
});

//Author page
Route::group(['namespace' => 'blog', 'prefix' => 'blog'], function(){
    Route::resource('authors', 'AuthorController')
        ->only('index', 'show')
        ->names('post.authors');
});
//Admin panel posts and categories pages
$groupData = [
    'namespace' => 'Blog\Admin',
    'prefix'    => 'admin/blog',
];
Route::group($groupData, function (){
    Route::resource('categories', 'CategoryController')->except('show')->middleware('is_admin')->names('blog.admin.categories');

    Route::resource('posts', 'PostController')->except('show')->middleware('is_admin')->names('blog.admin.posts');
});

